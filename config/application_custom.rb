module Consul
  class Application < Rails::Application
    config.i18n.default_locale = :es
	config.time_zone = 'London'
  end
end
