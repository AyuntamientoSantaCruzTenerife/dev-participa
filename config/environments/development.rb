Rails.application.configure do
  # Settings specified here will take precedence over those in config/application.rb.

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = true

  # Do not eager load code on boot.
  config.eager_load = true

  # Show full error reports and disable caching.
  config.consider_all_requests_local       = false
  config.action_controller.perform_caching = true

  # Don't care if the mailer can't send.
  config.action_mailer.raise_delivery_errors = false
  config.action_mailer.default_url_options = { host: 'https://www.santacruzdetenerife.es'}
  config.action_mailer.asset_host = "https://www.santacruzdetenerife.es"

  # Deliver emails to a development mailbox at /letter_opener
  # config.action_mailer.delivery_method = :letter_opener
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
  address:              'correo.santacruzdetenerife.es',
  port:                 25,
  domain:               'santacruzdetenerife.es',
  #user_name:            'participa@santacruzdetenerife.es',
  #password:             'SctfeParticipa2017',
  #authentication:       'plain',
  enable_starttls_auto: false  }

  #config.action_controller.asset_host = 'participa'

  # Print deprecation notices to the Rails logger.
  config.active_support.deprecation = :log

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = false

  # Asset digests allow you to set far-future HTTP expiration dates on all assets,
  # yet still be able to expire them through the digest params.
  config.assets.digest = true

 config.assets.compile = true


  # Adds additional error checking when serving assets at runtime.
  # Checks for improperly declared sprockets dependencies.
  # Raises helpful error messages.
  config.assets.raise_runtime_errors = true

  # Raises error for missing translations
  # config.action_view.raise_on_missing_translations = true

  config.cache_store = :dalli_store

 # Force all access to the app over SSL, use Strict-Transport-Security, and use secure cookies.
 config.force_ssl = true  



  config.after_initialize do
    Bullet.enable = true
    Bullet.bullet_logger = true
    if ENV['BULLET']
      Bullet.rails_logger = true
      Bullet.add_footer = true
    end
  end
end
